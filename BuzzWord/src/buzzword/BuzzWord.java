package buzzword;

import apptemplate.AppTemplate;
import components.AppComponentsBuilder;
import components.AppDataComponent;
import components.AppFileComponent;
import components.AppWorkspaceComponent;
import data.BuzzWordData;
import data.BuzzWordGameModeFile;
import data.BuzzWordPlayerFile;
import gui.Gamespace;

/**
 * @author Jenny Li
 */
public class BuzzWord extends AppTemplate {

    public static void main(String[] args) {
        launch(args);
    }

    public String getFileControllerClass() {
        return "BuzzWordController";
    }

    @Override
    public AppComponentsBuilder makeAppBuilderHook() {
        return new AppComponentsBuilder() {
            @Override
            public AppDataComponent buildDataComponent() throws Exception {
                return new BuzzWordData(BuzzWord.this);
            }

            @Override
            public AppFileComponent buildPlayerFileComponent() throws Exception {
                return new BuzzWordPlayerFile();
            }

            @Override
            public AppFileComponent buildGameModeFileComponent() throws Exception {
                return new BuzzWordGameModeFile();
            }

            @Override
            public AppWorkspaceComponent buildWorkspaceComponent() throws Exception {
                return new Gamespace(BuzzWord.this);
            }
        };
    }
}
