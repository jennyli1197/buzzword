package controller;

import apptemplate.AppTemplate;
import data.BuzzWordData;
import data.BuzzWordPlayerFile;
import gui.Gamespace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import static java.lang.Character.isLetter;
import static java.lang.Character.toLowerCase;
import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;


/**
 * @author Jenny Li
 */
public class BuzzWordController implements FileController {

    private static long nanoseconds = 1000000000;

    private AppTemplate appTemplate;
    private BuzzWordData gameData;
    private boolean login;
    private boolean inPlay;
    private boolean paused;
    private boolean levelCleared;
    private Path playerFile;
    private ArrayList<Integer> previousNodes;

    public BuzzWordController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        login = false;
        inPlay = false;
        paused = false;
        playerFile = null;
    }

    public void startLevel() {
        Gamespace gamespace = (Gamespace)appTemplate.getWorkspaceComponent();
        ensureCorrectWorkspace();
        gamespace.layoutGameplayScreen();

        BorderPane gameplayPane = gamespace.getGameplayPane();
        StackPane linePane = (StackPane)gameplayPane.getCenter();
        GridPane letterPane = (GridPane)linePane.getChildren().get(4);
        VBox rightPane = (VBox)gameplayPane.getRight();
        HBox targetPane = (HBox)rightPane.getChildren().get(3);
        VBox botPane = (VBox)gameplayPane.getBottom();
        Text currentLevel = (Text)botPane.getChildren().get(0);
        currentLevel.setText("Level "+gameData.getPlayLevel());

        String sixteenLetters = makeSixteenLetters();
        ArrayList<int[]> neighbors = getNeighbors();
        makeLetterGrid(letterPane);
        int pos = new Random().nextInt(16);
        int sixteenLettersPos = 0;
        while (sixteenLettersPos < sixteenLetters.length()) {
            StackPane pane = (StackPane)letterPane.getChildren().get(pos);
            Button button = (Button)pane.getChildren().get(1);
            if (button.getText().isEmpty()) {
                button.setText(""+sixteenLetters.charAt(sixteenLettersPos));
                moveNeighborToBack(neighbors, pos);
                pos = neighbors.get(pos)[0];
                sixteenLettersPos++;
            }
            else {
                moveNeighborToBack(neighbors, pos);
                pos = neighbors.get(pos)[0];
            }
        }

        makeTargetWordList(letterPane, gameData.getPlayLevel());
        Text targetPoints = (Text)targetPane.getChildren().get(1);
        targetPoints.setText(""+gameData.getTargetScore());

        play();
    }

    private void pauseGame() {
        paused = true;
        Gamespace gamespace = (Gamespace)appTemplate.getWorkspaceComponent();
        Button pauseButton = gamespace.getPauseButton();
        gamespace.layoutFakeScreen();
        pauseButton.setText("Resume Game");
    }

    private void resumeGame() {
        paused = false;
        Gamespace gamespace = (Gamespace)appTemplate.getWorkspaceComponent();
        Button pauseButton = gamespace.getPauseButton();
        BorderPane gameplayPane = gamespace.getGameplayPane();
        StackPane linePane = (StackPane)gameplayPane.getCenter();
        linePane.getChildren().remove(5);
        pauseButton.setText("Pause Game");
    }

    private void showEndGameScreen() {
        PropertyManager props = PropertyManager.getManager();

        Gamespace gamespace = (Gamespace)appTemplate.getWorkspaceComponent();
        gamespace.layoutEndScreen();
        BorderPane gameplayPane = gamespace.getGameplayPane();
        StackPane linePane = (StackPane)gameplayPane.getCenter();
        StackPane endScreen = (StackPane)linePane.getChildren().get(5);
        StackPane textPane = (StackPane)endScreen.getChildren().get(0);
        Text endGameText = (Text)textPane.getChildren().get(0);
        if (levelCleared)
            endGameText.setText(props.getPropertyValue(GAME_WON_MESSAGE));
        else
            endGameText.setText(props.getPropertyValue(GAME_LOST_MESSAGE));
    }

    private String makeSixteenLetters() {
        ArrayList<String> wordList = (ArrayList)gameData.getWordList();

        String sixteenLetters = "";
        ArrayList<String> possibleWords = new ArrayList<>();
        do {
            int rand = new Random().nextInt(wordList.size());
            String word = wordList.get(rand);
            possibleWords.add(word);
            for (int i = 0; i < possibleWords.size(); i++) {
                if (word.startsWith(possibleWords.get(i)))
                    sixteenLetters += word.substring(word.lastIndexOf(possibleWords.get(i)));
                else if (word.endsWith(possibleWords.get(i)))
                    sixteenLetters += word.substring(0, word.indexOf(possibleWords.get(i)));
            }
        } while (sixteenLetters.length() < 16);
        if (sixteenLetters.length() > 15)
            sixteenLetters = sixteenLetters.substring(0,16);
        return sixteenLetters;
    }

    private ArrayList<int[]> getNeighbors() {
        ArrayList<int[]> neighbors = new ArrayList<>();
        neighbors.add(new int[]{1, 5, 4});                      // 0
        neighbors.add(new int[]{0, 4, 5, 6, 2});                // 1
        neighbors.add(new int[]{3, 7, 6, 5, 1});                // 2
        neighbors.add(new int[]{2, 6, 7});                      // 3
        neighbors.add(new int[]{0, 1, 5, 9, 8});                // 4
        neighbors.add(new int[]{2, 1, 0, 4, 8, 9, 10, 6});      // 5
        neighbors.add(new int[]{1, 2, 3, 7, 11, 10, 9, 5});     // 6
        neighbors.add(new int[]{3, 2, 6, 10, 11});              // 7
        neighbors.add(new int[]{12, 13, 9, 5, 4});              // 8
        neighbors.add(new int[]{12, 13, 14, 10, 6, 5, 4, 8});   // 9
        neighbors.add(new int[]{15, 14, 13, 11, 7, 6, 5, 9});   // 10
        neighbors.add(new int[]{14, 15, 7, 6, 10});             // 11
        neighbors.add(new int[]{8, 9, 13});                     // 12
        neighbors.add(new int[]{10, 9, 8, 12, 14});             // 13
        neighbors.add(new int[]{9, 10, 11, 15, 13});            // 14
        neighbors.add(new int[]{11, 10, 14});                   // 15
        return neighbors;
    }

    private void moveNeighborToBack(ArrayList<int[]> list, int pos) {
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.get(i).length; j++) {
                if (list.get(i)[j] == pos) {
                    int intToMove = list.get(i)[j];
                    for (int k = j; k < list.get(i).length - 1; k++)
                        list.get(i)[k] = list.get(i)[k + 1];
                    list.get(i)[list.get(i).length - 1] = intToMove;
                }
            }
        }
    }

    private void makeLetterGrid(GridPane letterPane) {
        previousNodes = new ArrayList<>();
        for (int i = 0; i < gameData.GRID_SIZE; i++) {
            for (int j = 0; j < gameData.GRID_SIZE; j++) {
                StackPane pane = new StackPane();
                Circle circle = new Circle(20, Color.DARKSLATEGRAY);
                pane.getChildren().add(circle);
                Button bubble = new Button();
                bubble.getStyleClass().add("letter-grid-button");
                enableMouseEvents(bubble, i, j);
                pane.getChildren().add(bubble);
                letterPane.add(pane, j, i);
            }
        }
    }

    private void enableMouseEvents(Button letterButton, int row, int column) {
        Gamespace gamespace = (Gamespace)appTemplate.getWorkspaceComponent();
        BorderPane gameplayPane = gamespace.getGameplayPane();
        StackPane linePane = (StackPane)gameplayPane.getCenter();
        GridPane letterPane = (GridPane)linePane.getChildren().get(4);
        VBox rightPane = (VBox)gameplayPane.getRight();
        HBox currentGuess = (HBox)rightPane.getChildren().get(1);

        letterButton.setOnMousePressed((MouseEvent me) -> {
            checkGuess();
            addToGuess(linePane, 4*row+column, row, column);

            Text t = new Text("" + letterButton.getText());
            t.setFill(Color.WHITE);
            currentGuess.getChildren().add(t);
            previousNodes.add(4*row+column);
        });

        letterPane.setOnDragDetected((MouseEvent me) -> letterPane.startFullDrag());

        letterButton.setOnDragDetected((MouseEvent me) -> letterButton.startFullDrag());

        letterButton.setOnMouseDragExited((MouseEvent me) -> {
            if (previousNodes.contains(4*row+column))
                return;
            previousNodes.add(4*row+column);
        });

        letterButton.setOnMouseDragEntered((MouseEvent me) -> {
            if (!previousNodes.isEmpty()) {
                if (previousNodes.contains(4 * row + column))
                    return;

                addToGuess(linePane, previousNodes.get(previousNodes.size() - 1), row, column);

                Text t = new Text("" + letterButton.getText());
                t.setFill(Color.WHITE);
                currentGuess.getChildren().add(t);
            }
        });

        letterButton.setOnMouseDragReleased((MouseEvent me) -> {
            checkGuess();
            previousNodes.clear();
        });

        letterButton.setOnMouseClicked((MouseEvent me) -> {
            addToGuess(linePane, 4*row+column, row, column);

            Text t = new Text("" + letterButton.getText());
            t.setFill(Color.WHITE);
            currentGuess.getChildren().add(t);
            checkGuess();
        });

        letterPane.setOnMouseDragReleased((MouseEvent me) -> {
            checkGuess();
            previousNodes.clear();
        });
    }

    private void checkGuess() {
        Gamespace gamespace = (Gamespace)appTemplate.getWorkspaceComponent();
        BorderPane gameplayPane = gamespace.getGameplayPane();
        StackPane linePane = (StackPane)gameplayPane.getCenter();
        GridPane letterPane = (GridPane)linePane.getChildren().get(4);
        VBox rightPane = (VBox)gameplayPane.getRight();
        HBox currentGuess = (HBox)rightPane.getChildren().get(1);

        char[][] arr = getLetterGrid(letterPane);
        String s = "";
        for (int i = 0; i < currentGuess.getChildren().size(); i++) {
            Text t = (Text) currentGuess.getChildren().get(i);
            s += t.getText();
        }
        if (!s.isEmpty()) {
            for (int i = 0; i < arr.length; i++) {
                for (int j = 0; j < arr[i].length; j++) {
                    if (arr[i][j] == s.charAt(0)) {
                        clearHighlight();
                    }
                }
            }
        }
        updateScore(s);
        currentGuess.getChildren().remove(0, currentGuess.getChildren().size());
    }

    private char[][] getLetterGrid(GridPane letterPane) {
        char[][] arr = new char[gameData.GRID_SIZE][gameData.GRID_SIZE];
        int child = 0;
        for (int i = 0; i < gameData.GRID_SIZE; i++) {
            for (int j = 0; j < gameData.GRID_SIZE; j++) {
                StackPane pane = (StackPane)letterPane.getChildren().get(child);
                Button button = (Button)pane.getChildren().get(1);
                arr[i][j] = button.getText().charAt(0);
                child++;
            }
        }
        return arr;
    }

    private void makeTargetWordList(GridPane letterPane, int level) {
        char[][] arr = getLetterGrid(letterPane);
        ArrayList<String> wordList = (ArrayList)gameData.getWordList();
        ArrayList targetWordList = new ArrayList<>();
        boolean[][] visited = new boolean[gameData.GRID_SIZE][gameData.GRID_SIZE];
        for (int i = 0; i < gameData.GRID_SIZE; i++) {
            for (int j = 0; j < gameData.GRID_SIZE; j++)
                recursiveSearchForWords(arr, i, j, "", level, visited, wordList, targetWordList);
        }
        gameData.setTargetWords(targetWordList);
    }

    private void recursiveSearchForWords(char[][] arr, int row, int column, String word, int level, boolean[][] visited, ArrayList<String> wordList, ArrayList<String> list) {
        word += arr[row][column];
        boolean[][] temp = new boolean[visited.length][visited[0].length];
        for (int i = 0; i < visited.length; i++)
            temp[i] = Arrays.copyOf(visited[i], visited[i].length);
        temp[row][column] = true;
        boolean startsWith = false;
        for (int i = 0; i < wordList.size(); i++)
            if (wordList.get(i).startsWith(word)) {
                startsWith = true;
                break;
            }
        if (!startsWith)
            return;
        else {
            switch (level) {
                case 1:
                    if (word.length() >= 3)
                        addWord(word, wordList, list);
                    break;
                case 2:
                    if (word.length() >= 4)
                        addWord(word, wordList, list);
                    break;
                case 3:
                    if (word.length() >= 5)
                        addWord(word, wordList, list);
                    break;
                case 4:
                    if (word.length() >= 6)
                        addWord(word, wordList, list);
                    break;
                case 5:
                    if (word.length() >= 7)
                        addWord(word, wordList, list);
                    break;
                case 6:
                    if (word.length() >= 8)
                        addWord(word, wordList, list);
                    break;
                case 7:
                    if (word.length() >= 9)
                        addWord(word, wordList, list);
                    break;
                case 8:
                    if (word.length() >= 10)
                        addWord(word, wordList, list);
                    break;
                default: break;
            }
        }
        if (0 <= row-1 && 0 <= column-1 && !temp[row-1][column-1])                      // up & left
            recursiveSearchForWords(arr, row-1, column-1, word, level, temp, wordList, list);
        if (0 <= row-1 && !temp[row-1][column])                                         // up
            recursiveSearchForWords(arr, row-1, column, word, level, temp, wordList, list);
        if (0 <= row-1 && column+1 < arr[row].length && !temp[row-1][column+1])         // up & right
            recursiveSearchForWords(arr, row-1, column+1, word, level, temp, wordList, list);
        if (column+1 < arr[row].length && !temp[row][column+1])                         // right
            recursiveSearchForWords(arr, row, column+1, word, level, temp, wordList, list);
        if (row+1 < arr.length && column+1 < arr[row].length && !temp[row+1][column+1]) // down & right
            recursiveSearchForWords(arr, row+1, column+1, word, level, temp, wordList, list);
        if (row+1 < arr.length && !temp[row+1][column])                                 // down
            recursiveSearchForWords(arr, row+1, column, word, level, temp, wordList, list);
        if (row+1 < arr.length && 0 <= column-1 && !temp[row+1][column-1])              // down & left
            recursiveSearchForWords(arr, row+1, column-1, word, level, temp, wordList, list);
        if (0 <= column-1 && !temp[row][column-1])                                      // left
            recursiveSearchForWords(arr, row, column-1, word, level, temp, wordList, list);
    }

    private void addWord(String word, ArrayList<String> wordList, ArrayList<String> list) {
        for (int i = 0; i < wordList.size(); i++)
            if (wordList.get(i).equals(word)) {
                if (!list.contains(word)) {
                    list.add(word);
                    System.out.println(word); // print statement
                }
            }
    }

    private void play() {
        Gamespace gamespace = (Gamespace)appTemplate.getWorkspaceComponent();
        BorderPane gameplayPane = gamespace.getGameplayPane();
        StackPane linePane = (StackPane)gameplayPane.getCenter();
        GridPane letterPane = (GridPane)linePane.getChildren().get(4);
        char[][] arr = getLetterGrid(letterPane);
        boolean[][] visited = new boolean[gameData.GRID_SIZE][gameData.GRID_SIZE];
        int[][] paths = {{100, 100, 100, 100}, {100, 100, 100, 100}, {100, 100, 100, 100}, {100, 100, 100, 100}};

        VBox rightPane = (VBox)gameplayPane.getRight();
        HBox time = (HBox)rightPane.getChildren().get(0);
        Text seconds = (Text)time.getChildren().get(1);
        HBox currentGuess = (HBox)rightPane.getChildren().get(1);

        inPlay = true;
        levelCleared = false;
        AnimationTimer timer = new AnimationTimer() {
            long startTime = 0;
            @Override
            public void handle(long now) {
                if ((now - startTime) >= nanoseconds && !paused) {
                    gameData.setTimer(gameData.getTimer()-1);
                    seconds.setText(gameData.getTimer()+" seconds");
                    startTime = now;
                }
                appTemplate.getGUI().getAppPane().getChildren().get(0).setOnKeyTyped((KeyEvent ke) -> {
                    if (!paused) {
                        char guess = ke.getCharacter().charAt(0);
                        String s = "";
                        for (int i = 0; i < currentGuess.getChildren().size(); i++) {
                            Text t = (Text) currentGuess.getChildren().get(i);
                            s += t.getText();
                        }
                        if (isLetter(guess)) {
                            guess = toLowerCase(guess);
                            s += guess;
                            clearHighlight();
                            for (int i = 0; i < arr.length; i++) {
                                for (int j = 0; j < arr[i].length; j++) {
                                    if (arr[i][j] == s.charAt(0))
                                        recursiveSearchToHighlight(arr, visited, paths, s, 0, i, j, linePane);
                                }
                            }
                            Text t = new Text("" + guess);
                            t.setFill(Color.WHITE);
                            currentGuess.getChildren().add(t);
                        } else if (guess == '\b') {
                            if (!currentGuess.getChildren().isEmpty()) {
                                clearHighlight();
                                s = s.substring(0, s.length() - 1);
                                if (!s.isEmpty()) {
                                    for (int i = 0; i < arr.length; i++) {
                                        for (int j = 0; j < arr[i].length; j++) {
                                            if (arr[i][j] == s.charAt(0)) {
                                                recursiveSearchToHighlight(arr, visited, paths, s, 0, i, j, linePane);
                                            }
                                        }
                                    }
                                }
                                currentGuess.getChildren().remove(currentGuess.getChildren().size() - 1);
                            }
                        } else if (guess == '\r') {
                            if (!s.isEmpty()) {
                                for (int i = 0; i < arr.length; i++) {
                                    for (int j = 0; j < arr[i].length; j++) {
                                        if (arr[i][j] == s.charAt(0)) {
                                            clearHighlight();
                                        }
                                    }
                                }
                                updateScore(s);
                                s = "";
                                currentGuess.getChildren().remove(0, currentGuess.getChildren().size());
                            }
                        }
                    }
                });
                if (gameData.getCurrentScore() >= gameData.getTargetScore()) {
                    levelCleared = true;
                    stop();
                }
                else if (gameData.getCurrentScore() < gameData.getTargetScore() && gameData.getTimer() == 0) {
                    levelCleared = false;
                    stop();
                }
            }

            @Override
            public void stop() {
                super.stop();
                if (inPlay) {
                    showEndGameScreen();
                    end();
                }
            }
        };
        timer.start();

        PropertyManager props = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();
        Button pauseButton = gamespace.getPauseButton();
        pauseButton.setOnAction(e -> {
            if (!paused)
                pauseGame();
            else
                resumeGame();
        });
        Button replayButton = gamespace.getReplayButton();
        replayButton.setOnAction(e -> {
            if (!paused && inPlay)
                pauseGame();
            yesNoCancelDialog.show(props.getPropertyValue(LEAVE_GAME_TITLE), props.getPropertyValue(LEAVE_GAME_MESSAGE));
            if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES)) {
                inPlay = false;
                timer.stop();
                gameData.setCurrentScore(0);
                gameData.setTimer(60);
                resumeGame();
                startLevel();
            }
            else {
                if (paused)
                    resumeGame();
            }
        });
        Button homeButton = gamespace.getHomeButton();
        homeButton.setOnAction(e -> {
            if (!paused && inPlay)
                pauseGame();
            yesNoCancelDialog.show(props.getPropertyValue(LEAVE_GAME_TITLE), props.getPropertyValue(LEAVE_GAME_MESSAGE));
            if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES)) {
                inPlay = false;
                timer.stop();
                resumeGame();
                handleHomeRequest();
            }
            else {
                if (paused)
                    resumeGame();
            }
        });
        Button profileButton = gamespace.getProfileButton();
        profileButton.setOnAction(e -> {
            if (!paused && inPlay)
                pauseGame();
            yesNoCancelDialog.show(props.getPropertyValue(LEAVE_GAME_TITLE), props.getPropertyValue(LEAVE_GAME_MESSAGE));
            if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES)) {
                inPlay = false;
                timer.stop();
                resumeGame();
                handleProfileSettingsRequest();
            }
            else {
                if (paused)
                    resumeGame();
            }
        });
        Button helpButton = gamespace.getHelpButton();
        helpButton.setOnAction(e -> {
            if (!paused && inPlay)
                pauseGame();
            yesNoCancelDialog.show(props.getPropertyValue(LEAVE_GAME_TITLE), props.getPropertyValue(LEAVE_GAME_MESSAGE));
            if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES)) {
                inPlay = false;
                timer.stop();
                resumeGame();
                handleHelpRequest();
            }
            else {
                if (paused)
                    resumeGame();
            }
        });
    }

    private void recursiveSearchToHighlight(char[][] arr, boolean[][] visited, int[][] paths, String s, int index, int row, int column, StackPane linePane) {
        boolean[][] temp = new boolean[visited.length][visited[0].length];
        for (int i = 0; i < visited.length; i++)
            temp[i] = Arrays.copyOf(visited[i], visited[i].length);
        temp[row][column] = true;

        int[][] validPaths = new int[paths.length][paths[0].length];
        for (int i = 0; i < paths.length; i++)
            validPaths[i] = Arrays.copyOf(paths[i], validPaths[i].length);

        if (arr[row][column] == s.charAt(index)) {
            validPaths[row][column] = index;
            if (index == s.length() - 1) {
                int counter = 0;
                int previousPos = 0;
                while (counter < s.length()) {
                    for (int i = 0; i < validPaths.length; i++) {
                        for (int j = 0; j < validPaths[i].length; j++) {
                            if (validPaths[i][j] == counter) {
                                if (counter == 0)
                                    previousPos = 4*i+j;
                                addToGuess(linePane, previousPos, i, j);
                                previousPos = 4*i+j;
                                counter++;
                            }
                        }
                    }
                }
                return;
            }
            if (0 <= row-1 && 0 <= column-1 && !temp[row-1][column-1])                      // up & left
                recursiveSearchToHighlight(arr, temp, validPaths, s, index+1, row-1, column-1, linePane);
            if (0 <= row-1 && !temp[row-1][column])                                         // up
                recursiveSearchToHighlight(arr, temp, validPaths, s, index+1, row-1, column, linePane);
            if (0 <= row-1 && column+1 < arr[row].length && !temp[row-1][column+1])         // up & right
                recursiveSearchToHighlight(arr, temp, validPaths, s, index+1, row-1, column+1, linePane);
            if (column+1 < arr[row].length && !temp[row][column+1])                         // right
                recursiveSearchToHighlight(arr, temp, validPaths, s, index+1, row, column+1, linePane);
            if (row+1 < arr.length && column+1 < arr[row].length && !temp[row+1][column+1]) // down & right
                recursiveSearchToHighlight(arr, temp, validPaths, s, index+1, row+1, column+1, linePane);
            if (row+1 < arr.length && !temp[row+1][column])                                 // down
                recursiveSearchToHighlight(arr, temp, validPaths, s, index+1, row+1, column, linePane);
            if (row+1 < arr.length && 0 <= column-1 && !temp[row+1][column-1])              // down & left
                recursiveSearchToHighlight(arr, temp, validPaths, s, index+1, row+1, column-1, linePane);
            if (0 <= column-1 && !temp[row][column-1])                                      // left
                recursiveSearchToHighlight(arr, temp, validPaths, s, index+1, row, column-1, linePane);
        }
        else {
            return;
        }
    }

    private void clearHighlight() {
        Gamespace gamespace = (Gamespace)appTemplate.getWorkspaceComponent();
        BorderPane gameplayPane = gamespace.getGameplayPane();
        StackPane linePane = (StackPane)gameplayPane.getCenter();
        GridPane letterPane = (GridPane)linePane.getChildren().get(4);
        GridPane horizontalLinePane = (GridPane)linePane.getChildren().get(0);
        GridPane verticalLinePane = (GridPane)linePane.getChildren().get(1);
        GridPane forwardDiagonalLinePane = (GridPane)linePane.getChildren().get(2);
        GridPane backwardDiagonalLinePane = (GridPane)linePane.getChildren().get(3);

        for (int i = 0; i < letterPane.getChildren().size(); i++) {
            StackPane pane = (StackPane) letterPane.getChildren().get(i);
            Circle circle = (Circle) pane.getChildren().get(0);
            circle.setEffect(null);
        }
        for (int i = 0; i < horizontalLinePane.getChildren().size(); i++) {
            Line line = (Line)horizontalLinePane.getChildren().get(i);
            line.setEffect(null);
        }
        for (int i = 0; i < verticalLinePane.getChildren().size(); i++) {
            Line line = (Line)verticalLinePane.getChildren().get(i);
            line.setEffect(null);
        }
        for (int i = 0; i < forwardDiagonalLinePane.getChildren().size(); i++) {
            Line line = (Line) forwardDiagonalLinePane.getChildren().get(i);
            line.setVisible(false);
            Line line2 = (Line) backwardDiagonalLinePane.getChildren().get(i);
            line2.setVisible(false);
        }
    }

    private void addToGuess(StackPane linePane, int previousPos, int i, int j) {
        GridPane horizontalLinePane = (GridPane)linePane.getChildren().get(0);
        GridPane verticalLinePane = (GridPane)linePane.getChildren().get(1);
        GridPane forwardDiagonalLinePane = (GridPane)linePane.getChildren().get(2);
        GridPane backwardDiagonalLinePane = (GridPane)linePane.getChildren().get(3);
        GridPane letterPane = (GridPane)linePane.getChildren().get(4);

        StackPane pane = (StackPane) letterPane.getChildren().get(4*i+j);
        Circle circle = (Circle)pane.getChildren().get(0);
        circle.setEffect(new DropShadow(10, Color.RED));
        Line line = null;
        switch (previousPos - (4*i+j)) {
            case 5: line = (Line)forwardDiagonalLinePane.getChildren().get(3*i+j); break;
            case 4: line = (Line)verticalLinePane.getChildren().get(4*i+j); break;
            case 3: line = (Line)backwardDiagonalLinePane.getChildren().get(3*i+(j-1)); break;
            case -1: line = (Line)horizontalLinePane.getChildren().get(3*i+(j-1)); break;
            case -5: line = (Line)forwardDiagonalLinePane.getChildren().get(3*(i-1)+(j-1)); break;
            case -4: line = (Line)verticalLinePane.getChildren().get(4*(i-1)+j); break;
            case -3: line = (Line)backwardDiagonalLinePane.getChildren().get(3*(i-1)+j); break;
            case 1: line = (Line)horizontalLinePane.getChildren().get(3*i+j); break;
            default: break;
        }
        if (line != null) {
            line.setVisible(true);
            line.setEffect(new DropShadow(2.5, Color.RED));
        }
    }

    private void removeFromGuess(StackPane linePane, int previousPos, int i, int j) {
        GridPane letterPane = (GridPane)linePane.getChildren().get(4);
        GridPane horizontalLinePane = (GridPane)linePane.getChildren().get(0);
        GridPane verticalLinePane = (GridPane)linePane.getChildren().get(1);
        GridPane forwardDiagonalLinePane = (GridPane)linePane.getChildren().get(2);
        GridPane backwardDiagonalLinePane = (GridPane)linePane.getChildren().get(3);

        StackPane pane = (StackPane) letterPane.getChildren().get(4 * i + j);
        Circle circle = (Circle) pane.getChildren().get(0);
        circle.setEffect(null);
            Line line = null;
            switch (previousPos - (4*i+j)) {
                case 5:
                    line = (Line)forwardDiagonalLinePane.getChildren().get(3*i+j);
                    line.setVisible(false);
                    break;
                case 4: line = (Line)verticalLinePane.getChildren().get(4*i+j); break;
                case 3:
                    line = (Line)backwardDiagonalLinePane.getChildren().get(3*i+(j-1));
                    line.setVisible(false);
                    break;
                case -1: line = (Line)horizontalLinePane.getChildren().get(3*i+(j-1)); break;
                case -5:
                    line = (Line)forwardDiagonalLinePane.getChildren().get(3*(i-1)+(j-1));
                    line.setVisible(false);
                    break;
                case -4: line = (Line)verticalLinePane.getChildren().get(4*(i-1)+j); break;
                case -3:
                    line = (Line)backwardDiagonalLinePane.getChildren().get(3*(i-1)+j);
                    line.setVisible(false);
                    break;
                case 1: line = (Line)horizontalLinePane.getChildren().get(3*i+j); break;
                default: break;
            }
            if (line != null) {
                line.setEffect(null);
            }
    }

    private void updateScore(String word) {
        ArrayList<String> targetWords = gameData.getTargetWords();

        Gamespace gamespace = (Gamespace)appTemplate.getWorkspaceComponent();
        BorderPane gameplayPane = gamespace.getGameplayPane();
        VBox rightPane = (VBox)gameplayPane.getRight();
        GridPane guessesPane = (GridPane)rightPane.getChildren().get(2);
        GridPane totalPane = (GridPane)guessesPane.getChildren().get(1);
        Text totalScore = (Text)totalPane.getChildren().get(1);

        if (targetWords.contains(word)) {
            int pointValue = gameData.getScores().get(word.length());
            addValidGuess(word, pointValue);
            gameData.setCurrentScore(gameData.getCurrentScore() + pointValue);
            totalScore.setText("" + gameData.getCurrentScore());
            targetWords.remove(word);
        }
    }

    private void addValidGuess(String word, int pointValue) {
        Gamespace gamespace = (Gamespace)appTemplate.getWorkspaceComponent();
        BorderPane gameplayPane = gamespace.getGameplayPane();
        VBox rightPane = (VBox)gameplayPane.getRight();
        GridPane guessesPane = (GridPane)rightPane.getChildren().get(2);
        ScrollPane guessesScrollPane = (ScrollPane)guessesPane.getChildren().get(0);
        VBox validGuesses = (VBox)guessesScrollPane.getContent();
        Text guessedWord = new Text(String.format("%-20s%d",word,pointValue).toUpperCase());
        guessedWord.setFont(Font.font("Courier New"));
        if (inPlay)
            guessedWord.setFill(Color.WHITE);
        else
            guessedWord.setFill(Color.LIGHTGREEN);
        validGuesses.getChildren().add(guessedWord);
    }

    private void end() {
        PropertyManager props = PropertyManager.getManager();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();

        appTemplate.getGUI().getAppPane().getChildren().get(0).setOnKeyTyped(null);
        inPlay = false;
        Gamespace gamespace = (Gamespace)appTemplate.getWorkspaceComponent();
        BorderPane gameplayPane = gamespace.getGameplayPane();
        FlowPane topPane = (FlowPane)gameplayPane.getTop();
        Text gameMode = (Text)topPane.getChildren().get(0);

        Button pauseButton = gamespace.getPauseButton();
        pauseButton.setVisible(false);
        ArrayList<String> targetWords = gameData.getTargetWords();
        for (int i = 0; i < targetWords.size(); i++) {
            addValidGuess(targetWords.get(i), gameData.getScores().get(targetWords.get(i).length()));
        }

        Button nextLevelButton = gamespace.getNextLevelButton();
        int index = gameData.getGameMode().indexOf(gameMode.getText());
        int currentMaxLevel = gameData.getCurrentLevels().get(index);
        if (levelCleared) {
            if (gameData.getPlayLevel() == currentMaxLevel) {
                if (currentMaxLevel < gameData.getTotalLevels().get(index))
                    gameData.getCurrentLevels().set(index, currentMaxLevel + 1);
            }
        }
        if (gameData.getPlayLevel() < currentMaxLevel) {
            nextLevelButton.setVisible(true);
            nextLevelButton.setOnAction(e -> {
                inPlay = false;
                gameData.setCurrentScore(0);
                gameData.setTimer(60);
                gameData.setPlayLevel(gameData.getPlayLevel() + 1);
                startLevel();
            });
        }

        int[] currentGameModeRecords = gameData.getRecords().get(index);
        if (currentGameModeRecords[gameData.getPlayLevel()-1] < gameData.getCurrentScore()) {
            currentGameModeRecords[gameData.getPlayLevel()-1] = gameData.getCurrentScore();
            Platform.runLater(() -> {
                if (dialog.isShowing())
                    dialog.toFront();
                else
                    dialog.show(props.getPropertyValue(NEW_RECORD_TITLE), props.getPropertyValue(NEW_RECORD_MESSAGE_INCOMPLETE)+""+gameData.getCurrentScore());
            });
        }

        try {
            appTemplate.getPlayerFileComponent().saveData(appTemplate.getDataComponent(), playerFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void ensureCorrectWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    @Override
    public void handleNewProfileRequest() {
        Gamespace gamespace = (Gamespace)appTemplate.getWorkspaceComponent();
        gamespace.layoutNewProfileScreen();

        TextField tfName = (TextField) gamespace.getNewProfilePane().getChildren().get(2);
        tfName.requestFocus();
        PasswordField pfPassword = (PasswordField)gamespace.getNewProfilePane().getChildren().get(3);

        gamespace.getNewProfilePane().setOnKeyPressed((KeyEvent e) -> {
            if (e.getCode().equals(KeyCode.ESCAPE)) {
                handleHomeRequest();
            }
        });
        Button saveProfileButton = gamespace.getSaveProfileButton();
        saveProfileButton.setOnAction(e -> {
            PropertyManager props = PropertyManager.getManager();
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();

            String playerName = tfName.getText();
            String password = pfPassword.getText();
            if (password.trim().isEmpty() || playerName.trim().isEmpty())
                dialog.show(props.getPropertyValue(PROFILE_SAVE_ERROR_TITLE), props.getPropertyValue(PROFILE_SAVE_ERROR_MESSAGE));
            else {
                Path appDirPath = Paths.get(props.getPropertyValue(APP_TITLE)).toAbsolutePath();
                Path targetPath = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
                targetPath.toFile().getParentFile().mkdirs();
                targetPath.toFile().mkdir();
                File file = new File(targetPath.toFile(), playerName + props.getPropertyValue(WORK_FILE_EXT));
                if (file.exists())
                    dialog.show(props.getPropertyValue(PROFILE_EXISTS_TITLE), props.getPropertyValue(PROFILE_EXISTS_MESSAGE));
                else {
                    playerFile = file.toPath();
                    gameData = (BuzzWordData) appTemplate.getDataComponent();
                    gameData.setName(playerName);
                    gameData.setPassword(password);

                    try {
                        handleSaveNewProfileRequest();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                    ensureCorrectWorkspace();
                    gamespace.layoutPlayerScreen();
                }
            }
        });
    }

    @Override
    public void handleProfileSettingsRequest() {
        Gamespace gamespace = (Gamespace)appTemplate.getWorkspaceComponent();
        ensureCorrectWorkspace();
        gamespace.layoutProfileSettingsScreen();
    }

    @Override
    public void handleLoginRequest() {
        Gamespace gamespace = (Gamespace)appTemplate.getWorkspaceComponent();
        playerFile = null;
        gamespace.layoutLoginScreen();

        PropertyManager props = PropertyManager.getManager();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();

        TextField tfName = (TextField)gamespace.getLoginScreen().getChildren().get(2);
        tfName.requestFocus();
        PasswordField pfPassword = (PasswordField)gamespace.getLoginScreen().getChildren().get(3);

        gamespace.getLoginScreen().setOnKeyPressed((KeyEvent e) -> {
            if (e.getCode().equals(KeyCode.ESCAPE)) {
                handleHomeRequest();
            }
            if (e.getCode().equals(KeyCode.ENTER)) {
                String playerName = tfName.getText();
                String password = pfPassword.getText();

                Path appDirPath = Paths.get(props.getPropertyValue(APP_TITLE)).toAbsolutePath();
                Path targetPath = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
                targetPath.toFile().getParentFile().mkdirs();
                targetPath.toFile().mkdir();
                File file = new File(targetPath.toFile() + "/" + playerName + props.getPropertyValue(WORK_FILE_EXT));

                BuzzWordPlayerFile player = (BuzzWordPlayerFile)appTemplate.getPlayerFileComponent();
                String playerPassword = "";

                if (password.trim().isEmpty() || playerName.trim().isEmpty())
                    dialog.show(props.getPropertyValue(LOGIN_ERROR_TITLE), props.getPropertyValue(LOGIN_ERROR_MESSAGE));
                else if (!file.exists())
                    dialog.show(props.getPropertyValue(PROFILE_NOT_EXIST_TITLE), props.getPropertyValue(PROFILE_NOT_EXIST_MESSAGE));
                else {
                    try {
                        playerPassword = player.loadPassword(appTemplate.getDataComponent(), file.toPath());
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                    if (!playerPassword.equals(password)) {
                        dialog.show(props.getPropertyValue(PASSWORD_ERROR_TITLE), props.getPropertyValue(PASSWORD_ERROR_MESSAGE));
                    }
                    else {
                        playerFile = file.toPath();
                        try {
                            handleLoadProfileRequest();
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }
                        ensureCorrectWorkspace();
                        gamespace.layoutPlayerScreen();
                    }
                }
            }
        });
    }

    public void handleLogoutRequest() {
        PropertyManager props = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();
        yesNoCancelDialog.show(props.getPropertyValue(LOGOUT_TITLE), props.getPropertyValue(LOGOUT_MESSAGE));
        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES)) {
            login = false;
            gameData.reset();
            handleHomeRequest();
        }
    }

    @Override
    public void handleNewGameRequest() {

    }

    @Override
    public void handleHomeRequest() {
        if (login) {
            Gamespace gamespace = (Gamespace)appTemplate.getWorkspaceComponent();
            ensureCorrectWorkspace();
            gamespace.layoutPlayerScreen();
        }
        else {
            appTemplate.getGUI().getAppPane().getChildren().clear();
            appTemplate.getGUI().getAppPane().getChildren().add(appTemplate.getGUI().getInitPane());
        }
    }

    @Override
    public void handleSaveNewProfileRequest() throws IOException {
        appTemplate.getPlayerFileComponent().saveData(appTemplate.getDataComponent(), playerFile);
        login = true;
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_PROFILE_COMPLETED_TITLE), props.getPropertyValue(SAVE_PROFILE_COMPLETED_MESSAGE));
    }

    @Override
    public void handleLoadProfileRequest() throws IOException {
        appTemplate.getPlayerFileComponent().loadData(appTemplate.getDataComponent(), playerFile);
        gameData = (BuzzWordData)appTemplate.getDataComponent();
        login = true;
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOGIN_COMPLETED_TITLE), props.getPropertyValue(LOGIN_COMPLETED_MESSAGE));
    }

    @Override
    public void handleLevelSelectRequest() {
        Gamespace gamespace = (Gamespace)appTemplate.getWorkspaceComponent();
        ensureCorrectWorkspace();
        gamespace.layoutLevelSelectionScreen();

        ComboBox cbGameMode = gamespace.getCbGameMode();
        gameData.makeWordList(cbGameMode.getValue().toString());
        cbGameMode.setOnAction(e -> updateGameModeRequest(cbGameMode));
    }

    private void updateGameModeRequest(ComboBox comboBox) {
        Gamespace gamespace = (Gamespace)appTemplate.getWorkspaceComponent();
        BorderPane levelSelectionPane = gamespace.getLevelSelectionPane();

        FlowPane topPane = (FlowPane)levelSelectionPane.getTop();
        Text gameMode = (Text)topPane.getChildren().get(0);
        gameMode.setText(comboBox.getValue().toString());
        gameData.makeWordList(gameMode.getText());

        GridPane levels = (GridPane)levelSelectionPane.getCenter();
        levels.getChildren().clear();
        gamespace.initializeLevelBubbles(levels);
    }

    @Override
    public void handlePauseRequest() {

    }

    @Override
    public void replayLevel() {

    }

    @Override
    public void handleNextLevelRequest() {

    }

    @Override
    public void handleHelpRequest() {
        Gamespace gamespace = (Gamespace)appTemplate.getWorkspaceComponent();
        ensureCorrectWorkspace();
        gamespace.layoutHelpScreen();
    }

    @Override
    public void handleExitRequest() {
        PropertyManager props = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();
        if (inPlay) {
            pauseGame();
            yesNoCancelDialog.show(props.getPropertyValue(EXIT_TITLE), props.getPropertyValue(EXIT_MESSAGE));
            if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
                System.exit(0);
            else
                resumeGame();
        }
        else
            System.exit(0);
    }

    public void handleEditProfileRequest() {
        Gamespace gamespace = (Gamespace)appTemplate.getWorkspaceComponent();
        gamespace.layoutEditProfileScreen();

        PasswordField pfOldPassword = (PasswordField) gamespace.getEditProfilePane().getChildren().get(2);
        pfOldPassword.requestFocus();
        PasswordField pfNewPassword = (PasswordField)gamespace.getEditProfilePane().getChildren().get(3);

        PropertyManager props = PropertyManager.getManager();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        gamespace.getEditProfilePane().setOnKeyPressed((KeyEvent e) -> {
            if (e.getCode().equals(KeyCode.ESCAPE)) {
                handleProfileSettingsRequest();
            }
            if (e.getCode().equals(KeyCode.ENTER)) {
                String oldPassword = pfOldPassword.getText();
                String newPassword = pfNewPassword.getText();

                Path appDirPath = Paths.get(props.getPropertyValue(APP_TITLE)).toAbsolutePath();
                Path targetPath = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
                targetPath.toFile().getParentFile().mkdirs();
                targetPath.toFile().mkdir();
                File file = new File(targetPath.toFile() + "/" + gameData.getName() + props.getPropertyValue(WORK_FILE_EXT));

                BuzzWordPlayerFile player = (BuzzWordPlayerFile) appTemplate.getPlayerFileComponent();
                String playerPassword = "";

                if (oldPassword.trim().isEmpty() || newPassword.trim().isEmpty())
                    dialog.show(props.getPropertyValue(PASSWORD_ERROR_TITLE), props.getPropertyValue(PASSWORD_ERROR_MESSAGE));
                else if (!file.exists())
                    dialog.show(props.getPropertyValue(PROFILE_NOT_EXIST_TITLE), props.getPropertyValue(PROFILE_NOT_EXIST_MESSAGE));
                else {
                    try {
                        playerPassword = player.loadPassword(appTemplate.getDataComponent(), file.toPath());
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                    if (!playerPassword.equals(oldPassword)) {
                        dialog.show(props.getPropertyValue(PASSWORD_ERROR_TITLE), props.getPropertyValue(PASSWORD_ERROR_MESSAGE));
                    } else {
                        playerFile = file.toPath();
                        gameData.setPassword(newPassword);
                        try {
                            appTemplate.getPlayerFileComponent().saveData(appTemplate.getDataComponent(), playerFile);
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }
                        ensureCorrectWorkspace();
                        gamespace.layoutProfileSettingsScreen();
                    }
                }
            }
        });
    }
}
