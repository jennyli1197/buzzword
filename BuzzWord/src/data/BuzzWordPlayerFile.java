package data;

import com.fasterxml.jackson.core.*;
import components.AppDataComponent;
import components.AppFileComponent;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

/**
 * @author Jenny Li
 */
public class BuzzWordPlayerFile implements AppFileComponent {

    public static final String NAME = "NAME";
    public static final String PASSWORD = "PASSWORD";
    public static final String GAME_MODE = "GAME_MODE";
    public static final String CURRENT_LEVEL = "CURRENT_LEVEL";
    public static final String RECORD = "RECORD";

    @Override
    public void saveData(AppDataComponent data, Path filePath) throws IOException {
        BuzzWordData gameData = (BuzzWordData)data;
        ArrayList<String> gameModes = gameData.getGameMode();
        ArrayList<Integer> currentLevels = gameData.getCurrentLevels();
        ArrayList<int[]> records = gameData.getRecords();

        JsonFactory jsonFactory = new JsonFactory();

        try (OutputStream out = Files.newOutputStream(filePath)) {

            JsonGenerator generator = jsonFactory.createGenerator(out, JsonEncoding.UTF8);

            generator.writeStartObject();

            generator.writeStringField(NAME, gameData.getName());

            /*Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, gameData.getKey());
            byte[] passwordBytes = cipher.doFinal(gameData.getPassword().getBytes());
            String password = new String(new BASE64Encoder().encode(passwordBytes));
            */
            byte[] passwordBytes = gameData.getPassword().getBytes("UTF8");
            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.ENCRYPT_MODE, gameData.getKey());
            String password = new BASE64Encoder().encode(cipher.doFinal(passwordBytes));
            generator.writeStringField(PASSWORD, password);

            generator.writeFieldName(GAME_MODE);
            generator.writeStartArray(gameModes.size());
            for (String s : gameModes)
                generator.writeString(s);
            generator.writeEndArray();

            generator.writeFieldName(CURRENT_LEVEL);
            generator.writeStartArray(currentLevels.size());
            for (Integer num : currentLevels)
                generator.writeNumber(num);
            generator.writeEndArray();

            generator.writeFieldName(RECORD);
            generator.writeStartArray(records.size());
            for (int i = 0; i < records.size(); i++) {
                generator.writeStartArray(records.get(i).length);
                for (int num : records.get(i))
                    generator.writeNumber(num);
                generator.writeEndArray();
            }
            generator.writeEndArray();

            generator.writeEndObject();

            generator.close();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public String loadPassword(AppDataComponent data, Path filePath) throws IOException {
        BuzzWordData gameData = (BuzzWordData)data;
        String password = "";

        JsonFactory jsonFactory = new JsonFactory();
        JsonParser  jsonParser  = jsonFactory.createParser(Files.newInputStream(filePath));

        while (!jsonParser.isClosed()) {
            JsonToken token = jsonParser.nextToken();
            if (JsonToken.FIELD_NAME.equals(token)) {
                String fieldname = jsonParser.getCurrentName();
                if (fieldname.equals(PASSWORD)) {
                    jsonParser.nextToken();
                    //password = jsonParser.getValueAsString();
                    try {
                        /*Cipher cipher = Cipher.getInstance("AES");
                        cipher.init(Cipher.DECRYPT_MODE, gameData.getKey());
                        byte[] passwordBytes = new BASE64Decoder().decodeBuffer(jsonParser.getValueAsString());
                        password = new String(cipher.doFinal(passwordBytes));*/
                        byte[] passwordByte = new BASE64Decoder().decodeBuffer(jsonParser.getValueAsString());
                        Cipher cipher = Cipher.getInstance("DES");
                        cipher.init(Cipher.DECRYPT_MODE, gameData.getKey());
                        password = new String(cipher.doFinal(passwordByte));
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    } catch (BadPaddingException e) {
                        e.printStackTrace();
                    } catch (IllegalBlockSizeException e) {
                        e.printStackTrace();
                    }
                }
                else
                    jsonParser.nextToken();
                }
            }
        return password;
    }

    @Override
    public void loadData(AppDataComponent data, Path filePath) throws IOException {
        BuzzWordData gameData = (BuzzWordData)data;
        gameData.reset();

        JsonFactory jsonFactory = new JsonFactory();
        JsonParser  jsonParser  = jsonFactory.createParser(Files.newInputStream(filePath));

        while (!jsonParser.isClosed()) {
            JsonToken token = jsonParser.nextToken();
            if (JsonToken.FIELD_NAME.equals(token)) {
                String fieldname = jsonParser.getCurrentName();
                switch (fieldname) {
                    case NAME:
                        jsonParser.nextToken();
                        gameData.setName(jsonParser.getValueAsString());
                        break;
                    case PASSWORD:
                        jsonParser.nextToken();
                        try {
                            /*Cipher cipher = Cipher.getInstance("AES");
                            cipher.init(Cipher.DECRYPT_MODE, gameData.getKey());
                            byte[] passwordBytes = new BASE64Decoder().decodeBuffer(jsonParser.getValueAsString());
                            String password = new String(cipher.doFinal(passwordBytes));*/
                            byte[] passwordByte = new BASE64Decoder().decodeBuffer(jsonParser.getValueAsString());
                            Cipher cipher = Cipher.getInstance("DES");
                            cipher.init(Cipher.DECRYPT_MODE, gameData.getKey());
                            String password = new String(cipher.doFinal(passwordByte));
                            gameData.setPassword(password);
                        } catch (NoSuchPaddingException e) {
                            e.printStackTrace();
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        } catch (InvalidKeyException e) {
                            e.printStackTrace();
                        } catch (BadPaddingException e) {
                            e.printStackTrace();
                        } catch (IllegalBlockSizeException e) {
                            e.printStackTrace();
                        }
                        break;
                    case GAME_MODE:
                        jsonParser.nextToken();
                        while (jsonParser.nextToken() != JsonToken.END_ARRAY)
                            gameData.addGameMode(jsonParser.getText());
                        break;
                    case CURRENT_LEVEL:
                        jsonParser.nextToken();
                        while (jsonParser.nextToken() != JsonToken.END_ARRAY)
                            gameData.addCurrentLevels(jsonParser.getIntValue());
                        break;
                    case RECORD:
                        jsonParser.nextToken();
                        int i = 0;
                        while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                            int[] arr = new int[gameData.getTotalLevels().get(i)];
                            int j = 0;
                            while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                                arr[j] = jsonParser.getIntValue();
                                j++;
                            }
                            i++;
                            gameData.addRecords(arr);
                        }
                        break;
                    default:
                        throw new JsonParseException(jsonParser, "Unable to load JSON data");
                }
            }
        }
        jsonParser.close();
    }

    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException {

    }
}
