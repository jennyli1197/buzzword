package data;

import components.AppDataComponent;
import components.AppFileComponent;

import java.io.IOException;
import java.nio.file.Path;

/**
 * @author Jenny Li
 */
public class BuzzWordGameModeFile implements AppFileComponent {

    public static final String GAME_MODE = "GAME_MODE";
    public static final String LEVEL = "LEVEL";
    public static final String TIMER = "TIMER";
    public static final String WORDS = "WORDS";
    public static final String LETTERS = "LETTERS";

    @Override
    public void saveData(AppDataComponent data, Path filePath) throws IOException {

    }

    @Override
    public void loadData(AppDataComponent data, Path filePath) throws IOException {

    }

    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException {

    }
}
