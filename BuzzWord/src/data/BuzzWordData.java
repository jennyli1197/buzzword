package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Jenny Li
 */
public class BuzzWordData implements AppDataComponent {

    public static final int GRID_SIZE = 4;
    private String name;
    private String password;
    private int timer;
    private ArrayList<String> gameModes;
    private ArrayList<Integer> totalLevels; // total levels of each game mode
    private ArrayList<Integer> currentLevels; // highest levels unlocked by player
    private List<String> wordList;
    private ArrayList<String> targetWords;
    private HashMap<Integer, Integer> scores;
    private int targetScore;
    private int playLevel; // level player is currently playing
    private int currentScore;
    private ArrayList<int[]> records;
    private String helpText;
    private Key key;
    private AppTemplate appTemplate;

    public BuzzWordData(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        makeKey();
        name = null;
        password = null;
        timer = 60;
        gameModes = new ArrayList<>();
        setGameModes();
        helpText = "";
        makeHelpText();
        totalLevels = new ArrayList<>();
        totalLevels.add(8);
        totalLevels.add(8);
        totalLevels.add(8);
        totalLevels.add(8);
        currentLevels = new ArrayList<>();
        currentLevels.add(1);
        currentLevels.add(1);
        currentLevels.add(1);
        currentLevels.add(1);
        records = new ArrayList<>();
        records.add(new int[]{0, 0, 0, 0, 0, 0, 0, 0});
        records.add(new int[]{0, 0, 0, 0, 0, 0, 0, 0});
        records.add(new int[]{0, 0, 0, 0, 0, 0, 0, 0});
        records.add(new int[]{0, 0, 0, 0, 0, 0, 0, 0});
        targetWords = new ArrayList<>();
        scores = new HashMap<>();
        setScores();
        targetScore = 0;
        currentScore = 0;
        playLevel = 0;
    }
    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public Key getKey() { return key; }

    public int getTimer() { return timer; }

    public void setTimer(int newTimer) { timer = newTimer; }

    public ArrayList<Integer> getTotalLevels() { return totalLevels; }

    public ArrayList<Integer> getCurrentLevels() { return currentLevels; }

    public ArrayList<String> getGameMode() { return gameModes; }

    public List<String> getWordList() { return wordList; }

    public HashMap<Integer, Integer> getScores() { return scores; }

    public int getTargetScore() { return targetScore; }

    public int getCurrentScore() { return currentScore; }

    public void setCurrentScore(int score) { currentScore = score; }

    public int getPlayLevel() { return playLevel; }

    public void setPlayLevel(int level) { playLevel = level; }

    public String getHelpText() { return helpText; }

    private void setGameModes() {
        URL gameModesResource = getClass().getClassLoader().getResource("words/Game Modes.txt");
        assert gameModesResource != null;

        try (Stream<String> lines = Files.lines(Paths.get(gameModesResource.toURI()))) {
            gameModes.addAll(lines.collect(Collectors.toList()));
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public List<String> makeWordList(String gameMode) {
        URL gameModesResource = getClass().getClassLoader().getResource("words/"+gameMode+".txt");
        assert gameModesResource != null;

        try (Stream<String> lines = Files.lines(Paths.get(gameModesResource.toURI()))) {
            wordList = lines.collect(Collectors.toList());
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }
        for (int i = 0; i < wordList.size(); i++) {
            String s = wordList.get(i).toLowerCase().replaceAll("\\s","");
            wordList.set(i, s);
        }
        return wordList;
    }

    private void setScores() {
        if (scores.isEmpty()) {
            scores.put(3, 5);
            scores.put(4, 5);
            scores.put(5, 10);
            scores.put(6, 10);
            scores.put(7, 15);
            scores.put(8, 15);
            scores.put(9, 20);
            scores.put(10, 20);
            scores.put(11, 25);
            scores.put(12, 25);
            scores.put(13, 30);
            scores.put(14, 30);
            scores.put(15, 35);
            scores.put(16, 35);
        }
    }

    private void makeTargetScore() {
        int maxPossibleScore = 0;
        for (int i = 0; i < targetWords.size(); i++) {
            String word = targetWords.get(i);
            maxPossibleScore += scores.get(word.length());
        }
        targetScore = (int)(maxPossibleScore*.75);
    }

    public ArrayList<int[]> getRecords() { return records; }

    void addCurrentLevels(Integer num) { currentLevels.add(num); }

    void addGameMode(String gameMode) { gameModes.add(gameMode); }

    void addRecords(int[] num) { records.add(num); }

    public void setTargetWords(ArrayList<String> list) {
        targetWords = list;
        makeTargetScore();
    }

    public ArrayList<String> getTargetWords() { return targetWords; }

    private void makeHelpText() {
        ArrayList<String> helpList = new ArrayList<>();
        URL gameModesResource = getClass().getClassLoader().getResource("words/Help.txt");
        assert gameModesResource != null;

        try (Stream<String> lines = Files.lines(Paths.get(gameModesResource.toURI()))) {
            helpList.addAll(lines.collect(Collectors.toList()));
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        for (int i = 0; i < helpList.size(); i++) {
            helpText += helpList.get(i);
            helpText += "\n";
        }
    }

    private void makeKey() {
        try {
            //key = KeyGenerator.getInstance("AES").generateKey();
            DESKeySpec keySpec = new DESKeySpec("secretpassword".getBytes("UTF8"));
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            key = keyFactory.generateSecret(keySpec);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void reset() {
        name = null;
        password = null;
        gameModes = new ArrayList<>();
        currentLevels = new ArrayList<>();
        records = new ArrayList<>();
        targetWords = new ArrayList<>();
        targetScore = 0;
        currentScore = 0;
        playLevel = 0;
    }
}
