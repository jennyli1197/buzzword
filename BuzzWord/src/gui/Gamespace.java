package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.BuzzWordController;
import data.BuzzWordData;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import propertymanager.PropertyManager;
import ui.AppGUI;

import static settings.AppPropertyType.APP_WINDOW_WIDTH;

/**
 * This class serves as the GUI component for the BuzzWord game.
 *
 * @author Jenny Li
 */
public class Gamespace extends AppWorkspaceComponent {

    private AppTemplate appTemplate;
    private AppGUI appGUI;

    private GridPane newProfilePane;
    private VBox profileSettingsPane;
    private GridPane editProfilePane;
    private GridPane loginScreen;
    private BorderPane levelSelectionPane;
    private BorderPane gameplayPane;
    private ScrollPane helpPane;
    private StackPane fakeScreen;
    private StackPane endScreen;
    private Button homeButton;
    private Button logoutButton;
    private ComboBox cbGameMode;
    private Button saveProfileButton;
    private Button profileButton;
    private Button startPlayingButton;
    private Button pauseButton;
    private Button replayButton;
    private Button nextLevelButton;
    Button levelSelectionButton;
    private Button helpButton;
    private Button exitButton;
    private BuzzWordController controller;

    public Gamespace(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        appGUI = this.appTemplate.getGUI();
        controller = (BuzzWordController)appGUI.getFileController();
    }

    public BorderPane getLevelSelectionPane() { return levelSelectionPane; }

    public GridPane getNewProfilePane() { return newProfilePane; }

    public GridPane getEditProfilePane() { return editProfilePane; }

    public Button getHomeButton() { return homeButton; }

    public Button getProfileButton() { return profileButton; }

    public GridPane getLoginScreen() { return loginScreen; }

    public VBox getProfileSettingsPane() { return profileSettingsPane; }

    public BorderPane getGameplayPane() { return gameplayPane; }

    public StackPane getFakeScreen() { return fakeScreen; }

    public Button getHelpButton() { return helpButton; }

    public Button getExitButton() { return exitButton; }

    public ComboBox getCbGameMode() { return cbGameMode; }

    public Button getStartPlayingButton() { return startPlayingButton; }

    public Button getSaveProfileButton() { return saveProfileButton; }

    public Button getPauseButton() { return pauseButton; }

    public Button getReplayButton() { return replayButton; }

    public Button getNextLevelButton() { return nextLevelButton; }

    private BorderPane layoutBuzzWordTopPane() {
        BorderPane buzzWordPane = new BorderPane();
        BorderPane topPane = new BorderPane();
        FlowPane topTopPane = new FlowPane();
        topTopPane.setAlignment(Pos.TOP_RIGHT);
        exitButton = new Button("X");
        exitButton.getStyleClass().add("exit-button");
        topTopPane.getChildren().add(exitButton);
        topPane.setTop(topTopPane);
        FlowPane topBotPane = new FlowPane();
        topBotPane.setAlignment(Pos.CENTER);
        Text title = new Text(appGUI.getApplicationTitle());
        title.setFont(Font.font(20));
        title.setFill(Color.WHITE);
        topBotPane.getChildren().add(title);
        topPane.setBottom(topBotPane);
        buzzWordPane.setTop(topPane);

        exitButton.setOnAction(e -> controller.handleExitRequest());

        return buzzWordPane;
    }

    public void layoutNewProfileScreen() {
        StackPane pane = new StackPane();

        newProfilePane = new GridPane();
        newProfilePane.setAlignment(Pos.CENTER);
        newProfilePane.setMaxSize(300, 130);
        newProfilePane.setHgap(10);
        newProfilePane.setVgap(10);
        newProfilePane.setStyle("-fx-background-color: rgb(0, 00, 0, .5);");
        Text name = new Text("Profile Name");
        name.setFill(Color.WHITE);
        Text password = new Text("Profile Password");
        password.setFill(Color.WHITE);
        TextField tfName = new TextField();
        PasswordField pfPassword = new PasswordField();
        saveProfileButton = new Button("Save");
        saveProfileButton.getStyleClass().add("buzz-word-pane-button");
        newProfilePane.add(name, 0, 0);
        newProfilePane.add(password, 0, 1);
        newProfilePane.add(tfName, 1, 0);
        newProfilePane.add(pfPassword, 1, 1);
        newProfilePane.add(saveProfileButton, 1, 2);
        pane.getChildren().add(newProfilePane);

        appGUI.getAppPane().getChildren().add(pane);
    }

    public void layoutProfileSettingsScreen() {
        BuzzWordData gameData = (BuzzWordData)appTemplate.getDataComponent();

        FlowPane toolbarPane = new FlowPane(Orientation.VERTICAL, 0, 10);
        toolbarPane.getStyleClass().add("toolbar");
        homeButton = appGUI.initializeChildButton(toolbarPane, "Home");
        workspace.getChildren().add(toolbarPane);

        BorderPane buzzWordPane = layoutBuzzWordTopPane();
        profileSettingsPane = new VBox(10);
        profileSettingsPane.setAlignment(Pos.TOP_CENTER);
        profileSettingsPane.setPadding(new Insets(60));

        HBox playerNameBox = new HBox(10);
        Text player = new Text("Player Name:");
        player.setFill(Color.WHITE);
        playerNameBox.getChildren().add(player);
        Text name = new Text(gameData.getName());
        playerNameBox.getChildren().add(name);
        profileSettingsPane.getChildren().add(playerNameBox);

        HBox passwordBox = new HBox(10);
        Text passwordText = new Text("Password:");
        passwordText.setFill(Color.WHITE);
        passwordBox.getChildren().add(passwordText);
        Text actualPassword = new Text(gameData.getPassword());
        passwordBox.getChildren().add(actualPassword);
        profileSettingsPane.getChildren().add(passwordBox);

        HBox modeLevelRecordBox = new HBox(10);

        VBox gameModeBox = new VBox(10);
        Text gameModeText = new Text("Game Mode");
        gameModeText.setUnderline(true);
        gameModeText.setFill(Color.WHITE);
        gameModeBox.getChildren().add(gameModeText);
        modeLevelRecordBox.getChildren().add(gameModeBox);

        VBox levelBox = new VBox(10);
        Text levelClearedText = new Text("Last Level Cleared");
        levelClearedText.setUnderline(true);
        levelClearedText.setFill(Color.WHITE);
        levelBox.getChildren().add(levelClearedText);
        modeLevelRecordBox.getChildren().add(levelBox);

        VBox recordBox = new VBox(10);
        Text recordText = new Text("Highest Score Obtained");
        recordText.setUnderline(true);
        recordText.setFill(Color.WHITE);
        recordBox.getChildren().add(recordText);
        modeLevelRecordBox.getChildren().add(recordBox);

        for (int i = 0; i < gameData.getGameMode().size(); i++) {
            gameModeBox.getChildren().add(new Text(gameData.getGameMode().get(i)));
            levelBox.getChildren().add(new Text(""+gameData.getCurrentLevels().get(i)));
            recordBox.getChildren().add(new Text(""+gameData.getRecords().get(i)[gameData.getCurrentLevels().get(i)-1]));
        }

        profileSettingsPane.getChildren().add(modeLevelRecordBox);

        Button editButton = new Button("Edit Profile");
        editButton.getStyleClass().add("buzz-word-pane-button");
        profileSettingsPane.getChildren().add(editButton);

        StackPane pane = new StackPane();
        pane.getChildren().add(profileSettingsPane); // hmmmm

        buzzWordPane.setCenter(pane);
        workspace.getChildren().add(buzzWordPane);
        workspace.setHgrow(buzzWordPane, Priority.ALWAYS);

        homeButton.setOnAction(e -> controller.handleHomeRequest());
        editButton.setOnAction(e -> controller.handleEditProfileRequest());
    }

    public void layoutEditProfileScreen() {
        StackPane pane = new StackPane();

        editProfilePane = new GridPane();
        editProfilePane.setAlignment(Pos.CENTER);
        editProfilePane.setMaxSize(300, 130);
        editProfilePane.setHgap(10);
        editProfilePane.setVgap(10);
        editProfilePane.setStyle("-fx-background-color: rgb(0, 00, 0, .5);");
        Text oldPassword = new Text("Old Password");
        oldPassword.setFill(Color.WHITE);
        Text newPassword = new Text("New Password");
        newPassword.setFill(Color.WHITE);
        PasswordField pfOldPassword = new PasswordField();
        PasswordField pfNewPassword = new PasswordField();
        editProfilePane.add(oldPassword, 0, 0);
        editProfilePane.add(newPassword, 0, 1);
        editProfilePane.add(pfOldPassword, 1, 0);
        editProfilePane.add(pfNewPassword, 1, 1);
        pane.getChildren().add(editProfilePane);

        appGUI.getAppPane().getChildren().add(pane);
    }

    public void layoutLoginScreen() {
        StackPane pane = new StackPane();

        loginScreen = new GridPane();
        loginScreen.setAlignment(Pos.CENTER);
        loginScreen.setMaxSize(300, 100);
        loginScreen.setVgap(10);
        loginScreen.setHgap(10);
        loginScreen.setStyle("-fx-background-color: rgb(0, 0, 0, .5);");
        Text name = new Text("Profile Name");
        name.setFill(Color.WHITE);
        Text password = new Text("Profile Password");
        password.setFill(Color.WHITE);
        TextField tfName = new TextField();
        PasswordField pfPassword = new PasswordField();
        loginScreen.add(name, 0, 0);
        loginScreen.add(password, 0, 1);
        loginScreen.add(tfName, 1, 0);
        loginScreen.add(pfPassword, 1, 1);
        pane.getChildren().add(loginScreen);

        appGUI.getAppPane().getChildren().add(pane);
    }

    public void layoutPlayerScreen() {
        BuzzWordData gameData = (BuzzWordData)appTemplate.getDataComponent();

        FlowPane toolbarPane = new FlowPane(Orientation.VERTICAL, 0, 10);
        toolbarPane.getStyleClass().add("toolbar");
        profileButton = appGUI.initializeChildButton(toolbarPane, gameData.getName());
        profileButton.setTooltip(new Tooltip("Edit Profile"));
        cbGameMode = new ComboBox();
        cbGameMode.getItems().addAll(gameData.getGameMode());
        cbGameMode.setValue("Select Mode");
        toolbarPane.getChildren().add(cbGameMode);
        startPlayingButton = appGUI.initializeChildButton(toolbarPane, "Start Playing");
        helpButton = appGUI.initializeChildButton(toolbarPane, "Help");
        logoutButton = appGUI.initializeChildButton(toolbarPane, "Logout");
        workspace.getChildren().add(toolbarPane);

        BorderPane buzzWordPane = layoutBuzzWordTopPane();
        GridPane centerPane = new GridPane();
        centerPane.setPadding(new Insets(60));
        centerPane.setAlignment(Pos.TOP_CENTER);
        centerPane.setVgap(20);
        centerPane.setHgap(20);
        appGUI.initializeBuzzWordChildren(centerPane);
        buzzWordPane.setCenter(centerPane);

        workspace.getChildren().add(buzzWordPane);
        workspace.setHgrow(buzzWordPane, Priority.ALWAYS);

        profileButton.setOnAction(e -> controller.handleProfileSettingsRequest());
        startPlayingButton.setOnAction(e -> controller.handleLevelSelectRequest());
        helpButton.setOnAction(e -> controller.handleHelpRequest());
        logoutButton.setOnAction(e -> controller.handleLogoutRequest());
        toolbarPane.setOnKeyPressed((KeyEvent e) -> {
            if (e.isControlDown() && e.getCode().equals(KeyCode.L)) {
                controller.handleLogoutRequest();
            }
        });
    }

    public void layoutLevelSelectionScreen() {
        BuzzWordData gameData = (BuzzWordData)appTemplate.getDataComponent();

        FlowPane toolbarPane = new FlowPane(Orientation.VERTICAL, 0, 10);
        toolbarPane.getStyleClass().add("toolbar");
        profileButton = appGUI.initializeChildButton(toolbarPane, gameData.getName());
        profileButton.setTooltip(new Tooltip("Edit Profile"));
        if (cbGameMode.getValue().equals("Select Mode"))
            cbGameMode.setValue(gameData.getGameMode().get(0));
        toolbarPane.getChildren().add(cbGameMode);
        homeButton = appGUI.initializeChildButton(toolbarPane, "Home");
        workspace.getChildren().add(toolbarPane);

        BorderPane buzzWordPane = layoutBuzzWordTopPane();
        levelSelectionPane = new BorderPane();

        FlowPane topPane = new FlowPane();
        topPane.setPadding(new Insets(30, 0, 0, 0));
        Text gameMode = new Text(cbGameMode.getValue().toString());
        gameMode.setFill(Color.WHITE);
        gameMode.setFont(Font.font(20));
        gameMode.setUnderline(true);
        topPane.getChildren().add(gameMode);
        topPane.setAlignment(Pos.CENTER);
        levelSelectionPane.setTop(topPane);

        GridPane levels = new GridPane();
        levels.setPadding(new Insets(30));
        levels.setAlignment(Pos.TOP_CENTER);
        levels.setHgap(20);
        levels.setVgap(20);
        initializeLevelBubbles(levels);
        levelSelectionPane.setCenter(levels);
        buzzWordPane.setCenter(levelSelectionPane);

        workspace.getChildren().add(buzzWordPane);
        workspace.setHgrow(buzzWordPane, Priority.ALWAYS);

        profileButton.setOnAction(e -> controller.handleProfileSettingsRequest());
        homeButton.setOnAction(e -> controller.handleHomeRequest());
    }

    public void initializeLevelBubbles(GridPane levels) { //edit this
        BuzzWordData gameData = (BuzzWordData)appTemplate.getDataComponent();
        int index = gameData.getGameMode().indexOf(cbGameMode.getValue().toString());

        int level = 1;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 4; j++) {
                Button bubble = new Button();
                bubble.setText("" + level);
                if (level <= gameData.getCurrentLevels().get(index)) {
                    bubble.getStyleClass().add("level-button-unlocked");
                    bubble.setOnAction(e -> {
                        gameData.setPlayLevel((int)bubble.getText().charAt(0)-48);
                        controller.startLevel();
                    });
                } else
                    bubble.getStyleClass().add("level-button-locked");
                levels.add(bubble, j, i);
                level++;
            }
        }
    }

    public void layoutGameplayScreen() {
        BuzzWordData gameData = (BuzzWordData)appTemplate.getDataComponent();

        FlowPane toolbarPane = new FlowPane(Orientation.VERTICAL, 0, 10);
        toolbarPane.getStyleClass().add("toolbar");
        profileButton = appGUI.initializeChildButton(toolbarPane, gameData.getName());
        profileButton.setTooltip(new Tooltip("Edit Profile"));
        homeButton = appGUI.initializeChildButton(toolbarPane, "Home");
        helpButton = appGUI.initializeChildButton(toolbarPane, "Help");
        workspace.getChildren().add(toolbarPane);

        BorderPane buzzWordPane = layoutBuzzWordTopPane();
        gameplayPane = new BorderPane();

        FlowPane topPane = new FlowPane();
        topPane.setPadding(new Insets(30, 0, 0, 0));
        Text gameMode = new Text(cbGameMode.getValue().toString());
        gameMode.setFill(Color.WHITE);
        gameMode.setFont(Font.font(20));
        gameMode.setUnderline(true);
        topPane.getChildren().add(gameMode);
        topPane.setAlignment(Pos.CENTER);
        gameplayPane.setTop(topPane);

        StackPane linePane = new StackPane();
        //linePane.setPadding(new Insets(30));
        //linePane.setAlignment(Pos.TOP_CENTER);

        GridPane horizontalLinePane = new GridPane();
        horizontalLinePane.setPadding(new Insets(50, 0, 0, 0));
        horizontalLinePane.setAlignment(Pos.TOP_CENTER);
        horizontalLinePane.setHgap(40);
        horizontalLinePane.setVgap(70);
        for (int i = 0; i < gameData.GRID_SIZE; i++) {
            for (int j = 0; j < gameData.GRID_SIZE-1; j++) {
                Line line = new Line(1, 30, 30, 30);
                line.setStroke(Color.DARKSLATEGRAY);
                horizontalLinePane.add(line, j, i);
            }
        }

        GridPane verticalLinePane = new GridPane();
        verticalLinePane.setPadding(new Insets(70, 0, 0, 0));
        verticalLinePane.setAlignment(Pos.TOP_CENTER);
        verticalLinePane.setHgap(70);
        verticalLinePane.setVgap(40);
        for (int i = 0; i < gameData.GRID_SIZE-1; i++) {
            for (int j = 0; j < gameData.GRID_SIZE; j++) {
                Line line = new Line(0, 0, 0, 29);
                line.setStroke(Color.DARKSLATEGRAY);
                verticalLinePane.add(line, j, i);
            }
        }

        GridPane forwardDiagonalLinePane = new GridPane();
        forwardDiagonalLinePane.setPadding(new Insets(64, 0, 0, 5));
        forwardDiagonalLinePane.setAlignment(Pos.TOP_CENTER);
        forwardDiagonalLinePane.setHgap(26);
        forwardDiagonalLinePane.setVgap(27);
        for (int i = 0; i < gameData.GRID_SIZE-1; i++) {
            for (int j = 0; j < gameData.GRID_SIZE-1; j++) {
                Line line = new Line(0, 0, 43, 41);
                line.setStroke(Color.DARKSLATEGRAY);
                line.setVisible(false);
                forwardDiagonalLinePane.add(line, j, i);
            }
        }

        GridPane backwardDiagonalLinePane = new GridPane();
        backwardDiagonalLinePane.setPadding(new Insets(65, 0, 0, 5));
        backwardDiagonalLinePane.setAlignment(Pos.TOP_CENTER);
        backwardDiagonalLinePane.setHgap(26);
        backwardDiagonalLinePane.setVgap(27);
        for (int i = 0; i < gameData.GRID_SIZE-1; i++) {
            for (int j = 0; j < gameData.GRID_SIZE-1; j++) {
                Line line = new Line(43, 0, 0, 41);
                line.setStroke(Color.DARKSLATEGRAY);
                line.setVisible(false);
                backwardDiagonalLinePane.add(line, j, i);
            }
        }
        linePane.getChildren().addAll(horizontalLinePane, verticalLinePane, forwardDiagonalLinePane, backwardDiagonalLinePane);

        // letter grid
        GridPane letterPane = new GridPane();
        letterPane.setPadding(new Insets(30));
        letterPane.setAlignment(Pos.TOP_CENTER);
        letterPane.setHgap(30);
        letterPane.setVgap(30);
        linePane.getChildren().add(letterPane);

        gameplayPane.setCenter(linePane);

        VBox rightPane = new VBox(10);
        rightPane.setPadding(new Insets(10, 10, 0, 0));

        // remaining time text
        HBox time = new HBox(5);
        time.getStyleClass().add("time-remain");
        Text timeRemain = new Text("TIME REMAINING:");
        timeRemain.setUnderline(true);
        timeRemain.setFill(Color.GRAY);
        time.getChildren().add(timeRemain);
        Text seconds = new Text(gameData.getTimer()+" seconds");
        seconds.setFill(Color.GRAY);
        time.getChildren().add(seconds);
        rightPane.getChildren().add(time);

        // guess text
        HBox currentGuess = new HBox(3);
        currentGuess.getStyleClass().add("guesses");
        rightPane.getChildren().add(currentGuess);

        // guesses box
        GridPane guessesPane = new GridPane();

        ScrollPane guessesScrollPane = new ScrollPane();
        guessesScrollPane.setPrefSize(200, 100);
        guessesScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        guessesScrollPane.getStyleClass().add("guesses-scroll-pane");

        VBox guesses = new VBox(5);
        guesses.setPadding(new Insets(5, 0, 5, 5));
        guessesScrollPane.vvalueProperty().bind(guesses.heightProperty());
        guessesScrollPane.setContent(guesses);

        GridPane totalPane = new GridPane();
        totalPane.setPadding(new Insets(5, 0, 5, 5));
        totalPane.getColumnConstraints().add(new ColumnConstraints(150));
        totalPane.getColumnConstraints().add(new ColumnConstraints(30));
        totalPane.getStyleClass().add("total-pane");
        Text totalWord = new Text("TOTAL");
        totalWord.setFill(Color.WHITE);
        totalPane.add(totalWord, 0, 0);
        Text totalScore = new Text("0");
        totalScore.setFill(Color.WHITE);
        totalPane.add(totalScore, 1, 0);

        guessesPane.add(guessesScrollPane, 0, 0);
        guessesPane.add(totalPane, 0, 1);
        rightPane.getChildren().add(guessesPane);

        // target points text
        HBox targetPane = new HBox(5);
        targetPane.getStyleClass().add("target-pane");
        Text target = new Text("TARGET:");
        target.setFill(Color.WHITE);
        target.setUnderline(true);
        targetPane.getChildren().add(target);
        Text targetPoints = new Text();
        targetPoints.setFill(Color.WHITE);
        targetPane.getChildren().add(targetPoints);
        rightPane.getChildren().add(targetPane);
        gameplayPane.setRight(rightPane);

        VBox botPane = new VBox(2);
        botPane.setPadding(new Insets(30));
        botPane.setAlignment(Pos.TOP_CENTER);

        Text currentLevel = new Text();
        currentLevel.setFont(Font.font(20));
        currentLevel.setFill(Color.WHITE);
        currentLevel.setUnderline(true);
        botPane.getChildren().add(currentLevel);

        HBox botBotPane = new HBox(5);
        botBotPane.setAlignment(Pos.TOP_CENTER);
        pauseButton = new Button("Pause Game");
        pauseButton.getStyleClass().add("game-button");
        pauseButton.setVisible(true);
        botBotPane.getChildren().add(pauseButton);

        replayButton = new Button("Restart Level");
        replayButton.getStyleClass().add("game-button");
        botBotPane.getChildren().add(replayButton);

        nextLevelButton = new Button("Next Level");
        nextLevelButton.getStyleClass().add("game-button");
        nextLevelButton.setVisible(false);
        botBotPane.getChildren().add(nextLevelButton);
        botPane.getChildren().add(botBotPane);
        gameplayPane.setBottom(botPane);

        buzzWordPane.setCenter(gameplayPane);
        workspace.getChildren().add(buzzWordPane);
        workspace.setHgrow(buzzWordPane, Priority.ALWAYS);
    }

    public void layoutHelpScreen() {
        BuzzWordData gameData = (BuzzWordData)appTemplate.getDataComponent();
        PropertyManager props = PropertyManager.getManager();
        FlowPane toolbarPane = new FlowPane(Orientation.VERTICAL, 0, 10);
        toolbarPane.getStyleClass().add("toolbar");
        homeButton = appGUI.initializeChildButton(toolbarPane, "Home");
        workspace.getChildren().add(toolbarPane);

        BorderPane buzzWordPane = layoutBuzzWordTopPane();
        helpPane = new ScrollPane();
        helpPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        helpPane.setPrefWidth(Integer.parseInt(props.getPropertyValue(APP_WINDOW_WIDTH))-175);
        helpPane.setFitToWidth(true);
        helpPane.getStyleClass().add("help-scroll-pane");
        Text helpText = new Text(gameData.getHelpText());
        helpText.setWrappingWidth(helpPane.getPrefWidth()-75);
        helpText.setFill(Color.WHITE);
        helpPane.setContent(helpText);
        buzzWordPane.setCenter(helpPane);
        workspace.getChildren().add(buzzWordPane);
        workspace.setHgrow(buzzWordPane, Priority.ALWAYS);

        homeButton.setOnAction(e -> controller.handleHomeRequest());
    }

    public void layoutFakeScreen() {
        fakeScreen = new StackPane();
        fakeScreen.setPadding(new Insets(20));
        fakeScreen.setAlignment(Pos.TOP_CENTER);
        Pane coloredPane = new Pane();
        coloredPane.setStyle("-fx-background-color: rgb(62, 77, 96);");
        fakeScreen.getChildren().add(coloredPane);
        StackPane linePane = (StackPane)gameplayPane.getCenter();
        linePane.getChildren().add(fakeScreen);
    }

    public void layoutEndScreen() {
        endScreen = new StackPane();
        endScreen.setPadding(new Insets(30));
        endScreen.setAlignment(Pos.CENTER);
        StackPane textPane = new StackPane();
        textPane.setMaxSize(300, 100);
        textPane.setStyle("-fx-background-color: rgb(0, 0, 0, .5);");
        Text endGameText = new Text();
        endGameText.setFill(Color.WHITE);
        endGameText.setFont(Font.font(20));
        textPane.getChildren().add(endGameText);
        endScreen.getChildren().add(textPane);
        StackPane linePane = (StackPane)gameplayPane.getCenter();
        linePane.getChildren().add(endScreen);
    }

    @Override
    public void initStyle() {

    }

    @Override
    public void reloadWorkspace() {

    }

    public void reinitialize() {

    }
}
